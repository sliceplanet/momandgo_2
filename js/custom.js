$(window).load(function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
    setTimeout(function() {
	   $('body').removeClass('loaded');
    }, 300);
    
   var viewport_wid = viewport().width;
	var viewport_height = viewport().height;


	if (viewport_wid <= 767) {

		var videoDetach = $('.js-video-detach').detach();
      videoDetach.appendTo('.js-video-block');
      var videoDetach3 = $('.js-slider-detach').detach();
      videoDetach3.appendTo('.js-slider-block');
          
   } else{
        // var videoDetach2 = $('.js-video-detach').detach();
        // videoDetach2.insertAfter('.product-descr__bages');
        // var videoDetach4 = $('.js-slider-detach').detach();
        // videoDetach4.insertAfter('.js-product-slider');
    };
	
	$('.product-slider-for__item').each(function(){
 		var imgBg = $(this).find('img').attr('src');
 		$(this).css({'background-image':'url(' + imgBg + ')'});
		var widSlider = $('.product-slider-for').width();
		$(this).width(widSlider-6);
 	});

 	$("header .navbar").addClass("navbar-fixed-top");

  	
   setTimeout (function(){      
   	$(".product-slider-nav .slick-prev").click(); 
   }, 500);
});

$(window).resize(function(){
   var viewport_wid = viewport().width;
	var viewport_height = viewport().height;

	if (viewport_wid <= 767) {
        var videoDetach = $('.js-video-detach').detach();
        videoDetach.appendTo('.js-video-block');
        var videoDetach3 = $('.js-slider-detach').detach();
        videoDetach3.appendTo('.js-slider-block');
	} else{
        var videoDetach2 = $('.js-video-detach').detach();
        videoDetach2.insertAfter('.js-product-descr__bages');
        var videoDetach4 = $('.js-slider-detach').detach();
        videoDetach4.insertAfter('.js-product-slider');
    };
	$('.product-slider-for__item').each(function(){
		var widSlider = $('.product-slider-for').width();
		$(this).width(widSlider-6);
 	});
});
/* viewport width */
function viewport(){
	var e = window,
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */
$(function(){
	/* placeholder*/
	$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){
 			$(this).attr('placeholder', placeholder);
 		});
 	});
	/* placeholder*/

	if ($('.post-preview__img-box_has-video').length) {
		$('.post-preview__img-box_has-video').click(function(){
			var cur_src = $(this).find('iframe');
			cur_src.attr('src', cur_src.attr('data-src') + '&amp;autoplay=1'),
			$(this).toggleClass('active');
			return false;
		});
	}

	if($('.styled').length) {
		$('.styled').styler();
	};

	$('[data-toggle="collapse"]').click(function(e) {
	    e.preventDefault();
	})


	/* filter accordion */
	// $('.collapse').collapse();
	/* end filter accordion */


	/* dropdowns */
	$('.dropdown').click(function(){
		if ($(window).width() < 1024) {
			if ($(this).hasClass('active')) {
				$(this).removeClass('active');
			} else {
				$('.dropdown').removeClass('active');
				$(this).addClass('active');
			}
		}
	});
	$(document).click(function(e){
		if (jQuery(e.target).parents().filter('.dropdown').length != 1) {
			$('.dropdown').removeClass('active');
		}
	});
	/* dropdowns */


	// $('.button-nav').click(function(){
	// 	$(this).toggleClass('active'),
	// 	$('.main-nav-list').slideToggle();
	// 	return false;
	// });

	/* components */
	if($('.brands__slider').length) {
		$('.brands__slider').slick({
			dots: false,
			arrows: true, //tmp
			infinite: true,
			speed: 300,
			autoplay: true,
  			autoplaySpeed: 2000,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 3
				  }
			  	},
				{
				  breakpoint: 600,
				  settings: {
					slidesToShow: 2
				  }
			  	},
				{
				  breakpoint: 400,
				  settings: {
					slidesToShow: 2
				  }
				}
			]
		});
	};
	if($('.experts__slider').length) {
		$('.experts__slider').slick({
			dots: false,
			arrows: true, //tmp
			infinite: true,
			speed: 300,
			autoplay: true,
  			autoplaySpeed: 2000,
			slidesToShow: 5,
			slidesToScroll: 1,
			responsive: [
				{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 4
				  }
			  	},
				{
				  breakpoint: 600,
				  settings: {
					slidesToShow: 3
				  }
			  	},
				{
				  breakpoint: 400,
				  settings: {
					slidesToShow: 2
				  }
				}
			]
		});
	};
	
	if ($('.product-slider-for')) {
		 $('.product-slider-for').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: false,
		  fade: true,
		  asNavFor: '.product-slider-nav'
		});
		$('.product-slider-nav').slick({
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  asNavFor: '.product-slider-for',
		  dots: false,
		  vertical: true,
		//   centerMode: true,
		  focusOnSelect: true,
		  responsive: [
			  {
				breakpoint: 480,
				settings: {
				  slidesToShow: 3
				}
			  }
		  ]
		});
	}
   
  	/* components */
    
    $('.button-up').click(function() {
		$('html, body').animate({scrollTop: 0}, 1000);
		return false;
	});
	$(window).scroll(function() {
		var scroll_num = $(window).scrollTop();
        var viewport_height = viewport().height;
		if (scroll_num > viewport_height/2) {			
			$(".button-up").show();					
		} else{
			$(".button-up").hide();
		}
	});
    $(window).load(function() {
		var scroll_num = $(window).scrollTop();
        var viewport_height = viewport().height;
		if (scroll_num > viewport_height/2) {			
			$(".button-up").show();					
		} else{
			$(".button-up").hide();
		}
	});

});



var handler = function(){

   if($('.product-slider-nav').length) {

    	setTimeout (function(){
   	   $('.product-slider-nav').slick("getSlick").refresh();
      }, 500);
  	   $('.product-slider-for').slick("getSlick").refresh();
   }
    
   var height_footer = $('footer').height();
	var height_header = $('header').height();

	var height_header_nav = $('header .navbar').height();
	var height_doc = $(document).height();

	var height_content = height_doc - height_footer - (height_header_nav + 20);
      
   var height_right_content = $('.right_content').height();

   $('.filters').css("position", "relative");
  
	
	//$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});
    
    if($('.colors-slider').length) {
		$('.colors-slider').slick({
			dots: false,
			arrows: true, //tmp
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 3
				  }
			  	},
				{
				  breakpoint: 768,
				  settings: {
					slidesToShow: 4
				  }
			  	}
			]
		});
	};

	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;

	if (viewport_wid <= 767) {
       
	} else{
        
    }

}
$(window).bind('load', handler);
$(window).bind('resize', handler);

$( document ).ready(function() {
 
});
